library(fields)
library(magrittr)
library(dplyr)
library(tidyr)
library(tidytext)
library(gutenbergr)
library(ggplot2)
library(gridExtra)
library(grid)
library(textdata)
library(reshape2)
library(wordcloud)
#---------------------
# some functions
#---------------------  
getNounBigrams <- function(data, keyWord){
  
  #data %>% filter(word2 == keyWord)%>% rename(word = word1) %>% mutate(word2=NULL) %>%
  #inner_join(get_sentiments("nrc")) %>% count(word, sentiment, sort = TRUE)
  
  data %>% filter(word2 == keyWord)%>% rename(word = word1) %>% mutate(word2=NULL) %>%
    inner_join(get_sentiments("bing")) %>% count(word, sentiment, sort = TRUE)
}

plotCloud <- function(data){
  if("negative" %in% data$sentiment &&  "positive" %in% data$sentiment){
    data  %>% acast(word ~ sentiment, value.var = "n", fill = 0) %>%
    comparison.cloud(title.size = 1, scale=c(2,.2), match.colors=TRUE,random.order=FALSE,
    title.bg.colors = "black", colors = c("red", "green3"))
  }
  else if(!("positive" %in% data$sentiment)){
    wordcloud(words = data$word, freq = data$n, scale=c(2,.2), random.order=FALSE, colors = "red")
  }


}
#====================================================================================
# Downloading book(s)
#====================================================================================
g <- gutenberg_works(languages = "en", all_languages = TRUE, only_languages = FALSE)
content <- gutenberg_download(159, mirror = "http://www.mirrorservice.org/sites/ftp.ibiblio.org/pub/docs/books/gutenberg/") #brunon, adam
book <- g[g$gutenberg_id %in% 159,c("gutenberg_id","title")]

#====================================================================================
# Analysing data
#====================================================================================
content %<>% left_join(book) %>% mutate(gutenberg_id = NULL)
bigrams <- content %>% unnest_tokens(bigram, text, token = "ngrams", n = 2) %>%
  separate(bigram, c("word1", "word2"), sep = " ") %>%
  filter(!word1 %in% stop_words$word, !word2 %in% stop_words$word)

nouns <- content %>% unnest_tokens(word, text) %>%
  filter(!word %in% stop_words$word) %>% left_join(parts_of_speech, by = c("word" = "word")) %>%
  filter(pos == "Noun")  %>% count(word, sort = TRUE)

#due to lame number of bigrams for 3 most common nouns i had to do some testing here 
#to get something at least "workable"

bigramMontgomery <- getNounBigrams(bigrams,"montgomery")
bigramIsland <- getNounBigrams(bigrams,"island")
bigramBeast <- getNounBigrams(bigrams,"beast")
#bigramMoreau <- getNounBigrams(bigrams,"moreau")
#bigramLaw <- getNounBigrams(bigrams,"law")
#bigramBeach <- getNounBigrams(bigrams,"beach")
#bigramHand <- getNounBigrams(bigrams,"hand")
#bigramTime <- getNounBigrams(bigrams,"time")
#bigramWhite <- getNounBigrams(bigrams,"white")
#bigramHead <- getNounBigrams(bigrams,"head")


montgomeryPlot <- plotCloud(bigramMontgomery)
islandPlot <- plotCloud(bigramIsland)
beastPlot <- plotCloud(bigramBeast) #don't know why "dead" doesn't plot here as well
