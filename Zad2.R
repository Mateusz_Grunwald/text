library(tm)
library(magrittr)
library(dplyr)
library(tidytext)
library(gsubfn)
library(reshape2)
library(ggplot2)
library(gridExtra)
library(grid)
#====================================================================================
# Loading & transforming data
#====================================================================================

data("acq")
data <- acq %>% tm_map(removePunctuation) %>% #remove punctation
tm_map(content_transformer(FUN = function(x) gsubfn(".", list('"' = "", "\n" = " "), x) )) #remove \n's & "

tdm <- TermDocumentMatrix(data, control = list(stopwords = TRUE, stemming = TRUE, removeNumbers = TRUE))
tdmTfIdf <- TermDocumentMatrix(data, control = list(stopwords = TRUE, stemming = TRUE, removeNumbers = TRUE, weighting = weightTfIdf))

wordMatrix <- as.matrix(tdm)
wordMatrixTfIdf <- as.matrix(tdmTfIdf)
#====================================================================================
# products& simmilarity
#====================================================================================
squareSum <- function(x){
  sqrt(sum(x^2)) 
} 
makeDot<-function(column,matrix)
{
  matrix[,column] %*% matrix
}
makeCosine<-function(column,matrix)
{
  val = (matrix[,column] %*% matrix/(squareSum(matrix[,column])*apply(matrix,2,squareSum))) %>%
    round(4) #to make it more "human friendly"
}

columns <- c(1:ncol(wordMatrix))

dotProduct <- sapply(columns,makeDot,matrix=wordMatrix)
dotProductTfIdf <- sapply(columns,makeDot,matrix=wordMatrixTfIdf)

cosineSimilarity <- sapply(columns,makeCosine,matrix=wordMatrix)
cosineSimilarityTfIdf <- sapply(columns,makeCosine,matrix=wordMatrixTfIdf)

#take only upper triangle, without diagonal
dotProduct[lower.tri(dotProduct,diag=TRUE)] <- 0
dotProductTfIdf[lower.tri(dotProductTfIdf,diag=TRUE)] <- 0
cosineSimilarity[lower.tri(cosineSimilarity,diag=TRUE)] <- 0
cosineSimilarityTfIdf[lower.tri(cosineSimilarityTfIdf,diag=TRUE)] <- 0

#====================================================================================
# visualisation
#====================================================================================
#melt matrices & converting to tibble to make them easier to plot
dotProduct <- as_tibble(melt(dotProduct)) %>% arrange(desc(value))
dotProductTfIdf <- as_tibble(melt(dotProductTfIdf)) %>% arrange(desc(value))
cosineSimilarity <- as_tibble(melt(cosineSimilarity)) %>% arrange(desc(value))
cosineSimilarityTfIdf <- as_tibble(melt(cosineSimilarityTfIdf)) %>% arrange(desc(value))

Plot <- function(tibble, filterLvl, title){
  head(tibble,filterLvl) %>% mutate(pair = sprintf("%d/%d", Var1, Var2), pairFactor = reorder(pair,value)) %>%
    ggplot() + geom_col(aes(value, pairFactor)) + ggtitle(title) + labs(x="value", y = "doc1/doc2")
} 
PlotHistogram <- function(tibble, title){
    ggplot(data = tibble) + geom_histogram(aes(x=value), bins=50)  + 
    coord_cartesian(ylim=c(1,NA)) + ggtitle(title)
} 

p1 <- Plot(dotProduct,20,"scalar product, word frequency")
p2 <- Plot(dotProductTfIdf,20,"scalar product, TF-IDF")
p3 <- Plot(cosineSimilarity,20,"cosine similarity, word frequency")
p4 <- Plot(cosineSimilarityTfIdf,20,"cosine similarity, TF-IDF")

print(grid.arrange(p1,p2,p3,p4, ncol=2))

h1 <- PlotHistogram(dotProduct,"scalar product histogram, word frequency")
h2 <- PlotHistogram(dotProductTfIdf,"scalar product histogram, TF-IDF")
h3 <- PlotHistogram(cosineSimilarity,"cosine similarity histogram, word frequency")
h4 <- PlotHistogram(cosineSimilarityTfIdf,"cosine similarity histogram, TF-IDF")

print(grid.arrange(h1,h2,h3,h4, ncol=2))

